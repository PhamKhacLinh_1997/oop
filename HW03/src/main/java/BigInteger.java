public class BigInteger
{
    String bigString;

    public BigInteger(long init) {
        bigString = String.valueOf(init);
    }

    public BigInteger(String init) {
        bigString = reduce(init);
    }

    public String toString() {
        return bigString;
    }

    public boolean equals(Object other) {
        if(bigString.compareTo(((BigInteger)(other)).toString()) == 0 )   return true;
        else                                                              return false;
    }

    public BigInteger clone() {
        return new BigInteger(this.toString());
    }

    public int compareTo(BigInteger other) {
        String s1 = bigString;
        String s2 = other.toString();

        if(s1.length() > s2.length())   return 1;
        if(s1.length() < s2.length())   return -1;
        if(s1.length() == s2.length())  {
            int len = s1.length();
            for(int i = 0; i <= len - 1; i++) {
                if(s1.charAt(i) > s2.charAt(i))   return 1;
                if(s1.charAt(i) < s2.charAt(i))   return -1;
            }
        }


        return 0;
    }

    public String reduce(String s) {
        int k = 0;
        int len = s.length();
        for(int i = 0; i <= len - 1; i++) {
            if(s.charAt(i) != '0') {
                break;
            }
            k++;
        }

        if(k == len )  k = k - 1;

        s = s.substring(k);

        return s;
    }

    public long toLong() {
        return Long.parseLong(bigString);
    }


    public BigInteger add(BigInteger other) {
        String s1 = bigString;
        String s2 = other.toString();
        boolean reverse  = false;

        if( s2.charAt(0) == '-' && s1.charAt(0) != '-') {
            s2 = s2.substring(1);
            BigInteger newother = new BigInteger(s2);
            return  this.subtract(newother);
        }

        if( s2.charAt(0) == '-' && s1.charAt(0) == '-' ) {
            s1 = s1.substring(1);
            s2 = s2.substring(1);
            reverse = true;
        }

        if( s2.charAt(0) != '-' && s1.charAt(0) == '-') {
            s1 = s1.substring(1);
            BigInteger newthis = new BigInteger(s1);
            return  other.subtract(newthis);
        }

        int len1 = s1.length();
        int len2 = s2.length();
        int len  = Math.max(len1, len2);

        int[] a   = new int[len];
        int[] b   = new int[len];
        int[] sum = new int[len];

        String Ssum;
        char[] c = new char[len];

        for(int i = 0; i <= len - 1; i++) {
            if(i < len1)    a[len - len1 + i] = (int)(s1.charAt(i)) - 48;


            if(i < len2)    b[len - len2 + i] = (int)(s2.charAt(i)) - 48;

        }

        int d = 0;
        for(int i = len - 1; i >= 0; i--) {
            sum[i] = a[i] + b[i] + d;

            if( sum[i] >= 10 )  {
                sum[i] = sum[i] % 10;
                d = 1;
            }
            else   d = 0;

            c[i] = Character.forDigit(sum[i], 10);
        }

        Ssum = new String(c);
        if( d == 1 )   Ssum = "1" + Ssum;
        if( reverse == true )   Ssum = "-" + Ssum;
        BigInteger result = new BigInteger(Ssum);

        return result;
    }

    public BigInteger subtract(BigInteger other) {
        int sign = 1;
        String s1 = bigString;
        String s2 = other.toString();

        if( s2.charAt(0) == '-' && s1.charAt(0) != '-') {
            s2 = s2.substring(1);
            BigInteger newother = new BigInteger(s2);
            return  this.add(newother);
        }

        if( s2.charAt(0) == '-' && s1.charAt(0) == '-' ) {
            s1 = s1.substring(1);
            s2 = s2.substring(1);
            if(sign == 1)    sign = -1;
            if(sign == -1)   sign = 1;
        }

        if( s2.charAt(0) != '-' && s1.charAt(0) == '-') {
            s2 = "-" + s2;
            BigInteger newother = new BigInteger(s2);
            return  other.add(newother);
        }

        if( this.compareTo(other) == -1 ) {
            s1 = s1 + s2;
            s2 = s1.substring(0, (s1.length() - s2.length()));
            s1 = s1.substring(s2.length());
            if(sign == 1)    sign = -1;
            if(sign == -1)   sign = 1;
        }


        int len1 = s1.length();
        int len2 = s2.length();
        int len  = Math.max(len1, len2);

        int[] a   = new int[len];
        int[] b   = new int[len];
        int[] sub = new int[len];

        String Ssub;
        char[] c = new char[len];

        for(int i = 0; i <= len - 1; i++) {
            if(i < len1)    a[len - len1 + i] = (int)(s1.charAt(i)) - 48;


            if(i < len2)    b[len - len2 + i] = (int)(s2.charAt(i)) - 48;

        }

        int d = 0;
        for(int i = len - 1; i >= 0; i--) {
            sub[i] = a[i] - b[i] - d;

            if( sub[i] < 0 )  {
                sub[i] = sub[i] + 10;
                d = 1;
            }
            else   d = 0;

            c[i] = Character.forDigit(sub[i], 10);
        }

        Ssub = new String(c);

        Ssub = reduce(Ssub);
        if( sign == -1 )    Ssub = "-" + Ssub;
        BigInteger result = new BigInteger(Ssub);
        return result;
    }
}
