import oop.util.*;


public class stack {
    class node{
        String value;
        public node(String init) {
            value = init;
        }
    }

    public static void main(String args[]) {
        Stack s = new Stack();

        s.push("Hello");
        String r = s.top();
        s.pop();
        System.out.println(r);
        System.out.println(s.isEmpty());
    }
}   