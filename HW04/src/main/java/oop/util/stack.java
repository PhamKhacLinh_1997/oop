
package oop.util;

public class Stack {
    private int c;
    private static final int b = (int) (1e5+7);
    private String r[] = new String[b];

    public Stack (){
        c = 0;
    }

    public boolean isEmpty() {
        if( this.size() == 0 )   return true;
        else                     return  false;
    }

    public void push (String s) {
        if(c >= b)
            System.out.println("Stack is full!");
        else {
            c++;
            r[c] = s;
        }
    }


    public void pop () {
        if(c > 0) {
            r[c] = "";
            c--;
        }
    }

    public int size (){
        return c;
    }


    public String top () {
        return r[c];
    }


}
