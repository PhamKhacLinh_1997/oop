import java.util.Date;

public class Person {
	String name;
	Date  birthday;
	
	Person(String _name, Date _birthday) {
		name      = _name;
		birthday  = _birthday;
	}
	
	public String toString() {
		return (name + " " + birthday) ;
	}
}