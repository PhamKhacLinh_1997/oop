

public class  Account {
	int balance;
	int count;
	int fee;
	Account( int _balance ) {
		balance    = _balance;
	    count      = 0;
		fee = 0;
	}
	
	boolean deposit(int monney) {
		int total = balance + monney;
        count++;
		
		if(total >= 0)  {
			balance = total;
			return true;
		}  
		
	    return false;
	}
	
	boolean withdraw(int monney)  {
		int remain = balance - monney;
        count++;
		
		if(remain >= 0)  {
			balance = remain;
			return true;
		}  
		
	    return false;
	}
	
	int endMonthCharge() {
		return fee;
	}
	
	void endMonth()   {
		StdOut.println("Balance = " + (balance - fee));
		StdOut.println("Transaction count = " + count);
		StdOut.println("Fee = " + fee);
		count = 0;
	}
}