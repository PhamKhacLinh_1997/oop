import java.util.Date;

public class Manager extends Employee {
	Employee assistant;
	
	Manager( String _name, Date _birthday, double _salary ) {
		super(_name, _birthday, _salary);
	} 
	
	void setAssistant( Employee _assistant ) {
		assistant = _assistant;
	}
	
	public String toString() {
		return super.toString() + " " + assistant.name ;
	}
}
