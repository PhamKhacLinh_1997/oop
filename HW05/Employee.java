import java.util.Date;

public class Employee extends Person {
	double salary;
	
	Employee( String _name, Date _birthday, double _salary ) {
		super(_name,_birthday);
		salary   = _salary;
	}
	
	public String toString() {
		return ( super.toString() + " " + salary );
	}
}
